$(function () {
    init();
})
function init() {
    $("#qhTable").bootstrapTable({
        height: "800px",
        url: "data.json"
    });
}

//用来
//用来
function myFormatter(value, row, index) {
    return "<button class=\"btn btn-success\" type=\"button\" onclick=\"editRow("+ index +")\"><i class=\"glyphicon glyphicon-list\" style=\"color:yellow\"></i></button>";
    // <input type='button' value='行编辑'>"
}
function editRow(i) {
    var currentRowData = $("#qhTable").data("bootstrap.table").options.data[i];
    var rowProperties = {};
    $("#qhTable thead tr th[data-field]").each(function () {
        var property=$(this).attr("data-field");
        rowProperties[property]='<div class="has-success"><input class="form-control" type="text" placeholder="Warning..." value="' + currentRowData[property] + '"></div>'
    })
    $("#qhTable").bootstrapTable('updateRow', {
        index: i, row: rowProperties
    });
}

function addRow(){
    var rowProperties = {};
    $("#qhTable thead tr th[data-field]").each(function () {
        var property=$(this).attr("data-field");
        rowProperties[property]='<div class="has-error"><input class="form-control" type="text" placeholder="Warning..." value=""></div>'
    })
    $("#qhTable").bootstrapTable('insertRow', {
        index: 0, row: rowProperties
    });
}

function totalFormatter(value, row, index) {
    var str = "<span style='color:red'>" + value + "</span>";
    if (value > 100) {
    } else {
        str = "<span style='color:lightseagreen'>" + value + "</span>"
    }
    return str;
}

function myQueryParams(params) {
    params["pageNum"] = (params.offset / params.limit) + 1;
    params.pageSize = params.limit;
    params.derection = "asc";
    params.sort = "id";
    params.orderCode = $("#orderCode").val() + "";
    return params;
}

function responseHandler(res) {
    var needData = {
        rows: res.row,
        total: res.total
    };
    return needData;
}
$(function () {
    $(".fixed-table-header table").css("display", "none");
    $('#qhTable').css("margin-top", "0px");
});
