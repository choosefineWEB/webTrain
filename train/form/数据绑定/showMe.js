$(function () {
    domInit();
    eventInit();
})

function domInit() {
    formInit();
}

function eventInit() {
}

function formInit() {
    var dataUrl = $("div[choosefine-url]").attr("choosefine-url");
    $.getJSON(dataUrl, function (data) {
        $("[choosefine-type=list]").each(function(){
            var listTpl=$(this);
            listTpl.prepend("{{each "+listTpl.attr("choosefine-field")+" as value index}}");
            listTpl.append("{{/each}}");
        })
        var source=$("div[choosefine-url]").html();
        var render = template.compile(source);
        var html = render(data);
        $("div[choosefine-url]").html(html);
    });
}

template.helper('arrayList', function (data,i) {
    return data[i%data.length];
});
