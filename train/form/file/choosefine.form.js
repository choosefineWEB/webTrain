$(function () {
    domInit();
    eventInit();
})

function domInit() {
    formInit();
}

function eventInit() {

}

function formInit() {
    var dataUrl = $("form").attr("data-url");
    $.getJSON(dataUrl, function (data) {
        $("form input[name]").each(function (i) {
            var inputName=$(this).attr("name");
            var inputValue=data[inputName];
            $(this).val(inputValue);
        });
        $("form select[name]").each(function (i) {
            var selectName=$(this).attr("name");
            var selectValue=data[selectName];
            var currentSelect=this;
            $(selectValue).each(function(){
                console.log(this);
                var option=$("<option></option>");
                option.attr("value",this.key);
                option.html(this.text);
                if(this.select=="true"){
                    option.attr("selected","selected");
                }
                $(currentSelect).append(option);
            });
        });
        $("form div[name][choosefine-type=list]").each(function (i) {
            var source=$(this).text();
            console.log("<%for(var i=0;key<i.length;i++){%>"+source+"<%}%>");
            var render = template.compile("<%for(var i=0;i<rows.length;i++){%>"+source+"<%}%>");
            var html = render(data);
            $(this).html(html);
        });
    });
}